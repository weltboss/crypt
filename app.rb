#!/usr/bin/env ruby

require 'sinatra/base'

  class App < Sinatra::Base
    get '/' do
      redirect 'login.html'
    end

    get '/home' do
      redirect 'home.html'
    end

    get '/incident' do
      redirect 'incident.html'
    end

  end

  
